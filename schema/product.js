var mongoose = require('mongoose')

var product = new mongoose.Schema({
    name: {
        required: true,
        type: String
    },
    product_Id: {
        required: true,
        type:Number
    },
    description: {
        type: String,
        required:true
    },
    prices: {
        type: Number,
        required:true
    },
    imageLink: {
        type: String,
        required: true
    }
})
module.export = product
