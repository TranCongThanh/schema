var buyer = require('./buyer')
var product = require('./product')
var user = require('./user')
var order = require('./order')

module.exports = {
    buyer,
    product,
    order
}