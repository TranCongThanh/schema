var mongoose = require('mongoose')

var buyer = new monogoose.Schema({
    name: {
      required: true,
      type: String,
    },
    location: {
      type: String,
    },
    numberPhone: {
      type: String
    },
    note: {
      type: String
    },
    orders: [{
      productId: {
        type: monogoose.Schema.Types.ObjectId,
        ref: "product"
      },
      name: {
        type: String
      },
      prices: {
        type: String
      },
      isPaied: {
        type: Boolean,
        default: false
      }
    }]
  });
  module.exports = buyer